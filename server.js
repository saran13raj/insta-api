const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();

const db = process.env.MONGO_URI;
const posts = require('./routes/api/posts');
const users = require('./routes/api/users');

const port = process.env.PORT || 4000;
const app = express();
// wcZ5vNgVsu8g4GUs

app.use(express.json());

// DB config

// connect to mongodb
mongoose
    .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
    })
    .then(() => {
        console.log('MongoDB connected');
    })
    .catch((err) => {
        console.log(err);
    });

app.use('/api/posts', posts);
app.use('/api/users', users);

app.listen(port, (err) => {
    if(err) {
        console.log(err);
    }
    console.log('Server running on port ' + port);
});