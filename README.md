# Insta API

Insta API is a simple repo to create, read, update and delete a post.

## Installation

Clone this repo. Open a terminal inside this repo and run th following command.

```bash
npm install
```

## Usage

Consume these api endpoints with a client like Postman.
