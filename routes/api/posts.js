const express = require('express');

const router = express.Router();

const Post = require('../../model/Post');

Array.prototype.remove = function (v) {
    if (this.indexOf(v) != -1) {
        this.splice(this.indexOf(v), 1);
        return true;
    }
    return false;
}

// get all posts
router.get('/', async (req, res) => {
    try {
        const posts = await Post.find({}).sort({date: -1});
        res.status(200).send(posts);
    } catch (error) {
        res.status(404).send({error});
    }
});

// get one post with id
router.get('/:id', async (req, res) => {
    const _id = req.params.id;
    try {
        const post = await Post.findById(_id);
        if(!post) {
            return res.status(404).send({error: 'Post not found'});
        }
        res.send(post);
    } catch (error) {  
        res.status(500).send({error: 'Internal server error'})
    }
});

// add new post
router.post('', async (req, res) => {
    const newPost = new Post(req.body);
    try {
        await newPost.save();
        res.status(201).send(newPost);
    } catch (error) {
        res.status(500).send({error: 'Internal server error'});
    }
    // newPost.save().then((post) => {
    //     res.status(201).send(post);
    // }).catch((err) => {
    //     res.status(500).send(err);
    // });
});

// add new comment
router.post('/:id', async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);
        if(!post) {
            return res.status(404).send({error: 'Post not found'})
        }
        console.log(post);
        post.comments.push(req.body);
        await post.save();
        res.send(post);
    } catch (error) {
        console.log(error);
        res.status(500).send({error: 'Internal server error'});
    }
    
})

// update post
router.patch('/:id', async (req, res) => {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['caption'];
    const isValidOperation = updates.every((update) => {
        return allowedUpdates.includes(update);
    });

    if(!isValidOperation) {
        return res.status(400).send({error: 'Invalid update'});
    }
    try {
        const post = await Post.findById(req.params.id);
        if(!post) {
            return res.status(404).send({error: 'Post not found'});
        }
        updates.forEach((update) => {
            post[update] = req.body[update];
        });
        await post.save();
        res.send(post);
    } catch (error) {
        res.status(500).send({error: 'Internal server error'});
    }
});

// add like to post
router.patch('/like/:id', async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);
        if(!post) {
            return res.status(404).send({error: 'Post not found'});
        }
        post.likes += 1;
        await post.save();
        res.send(post);
    } catch (error) {
        res.status(500).status({error: 'Internal server error'});
    }
});

// remove like from post
router.patch('/dislike/:id', async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);
        if(!post) {
            return res.status(404).send({error: 'Post not found'});
        }
        post.likes -= 1;
        await post.save();
        res.send(post);
    } catch (error) {
        res.status(500).status({error: 'Internal server error'});
    }
});

// update comment
router.patch('/:id/:c_id', async (req, res) => {
    // console.log(req.params);
    const updates = Object.keys(req.body);
    const allowedUpdates = ['comment'];
    const isValidOperation = updates.every((update) => {
        return allowedUpdates.includes(update);
    });

    if(!isValidOperation) {
        return res.status(400).send({error: 'Invalid update'});
    }
    try {
        const post = await Post.findById(req.params.id);
        if(!post) {
            return res.status(404).send({error: 'Post not found'});
        }
        const comment = post.comments.find((comment) => {
            return String(comment._id) === req.params.c_id;
        });
        console.log(comment);
        if(!comment) {
            return res.status(404).send({error: 'Comment not found'});
        }
        updates.forEach((update) => {
            post.comments.map(comment => {
                if(String(comment._id) === req.params.c_id) {
                    comment[update] = req.body[update];
                }
            });
        });
        await post.save();
        res.send(post);
    } catch (error) {
        res.status(500).send({error: 'Internal server error'});
    }
});

// delete post with id
router.delete('/:id', async (req, res) => {
    try {
        const post = await Post.findByIdAndDelete(req.params.id);

        if(!post) {
            return res.status(404).send({error: 'Post not found'});
        }
        res.send(post);
    } catch (error) {
        res.status(500).send({error: 'Internal server error'})
    }
    // Post.findById(req.params.id).then((post) => {
    //     post.remove().then(() => {
    //         res.status(200).send(post)
    //     }).catch((err) => {
    //         res.status(500).send(err)
    //     });
    // }).catch((err) => {
    //     res.status(404).send(err);
    // });
});

// delete comment with id
router.delete('/:id/:c_id', async (req, res) =>{
    try {
        const post = await Post.findById(req.params.id);
        if(!post) {
            return res.status(404).send({error: 'Post not found'});
        }
        post.comments = post.comments.remove(req.params.c_id);
        await post.save();
        res.send(post);
    } catch (error) {
        res.status(500).send({error: 'Internal server error'});
    }
});

module.exports = router;