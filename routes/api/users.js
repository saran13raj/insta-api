const express = require('express');
const bcrypt = require('bcryptjs');
require('dotenv').config();
const jwt = require('jsonwebtoken');

const router = express.Router();

const User = require('../../model/User');

// get all users
router.get('/', async (req, res) => {
    try {
        const users = await User.find({}).sort({date: -1});
        res.status(200).send(users);
    } catch (error) {
        res.status(404).send({error});
    }
});

// add new post
router.post('', async (req, res) => {
    const newUser = new User(req.body);
    try {
        if(!newUser.username || !newUser.name || !newUser.password) {
            return res.status(400).send({error: 'Enter all required fields'})
        }

        const user = await User.findOne({username: newUser.username});
        if(user) {
            return res.status(400).send({error: 'User already exists'})
        }

        bcrypt.genSalt(10, (err, salt) => {
            if(err) throw err;
            bcrypt.hash(newUser.password, salt, async (err, hash) => {
                if(err) throw err;
                newUser.password = hash;
                await newUser.save();
                jwt.sign(
                    {id: newUser._id},
                    process.env.JWT_SECRET,
                    {expiresIn: 3600},
                    (err, token) => {
                        if(err) throw err;
                        res.status(201).send({
                            token,
                            user: {
                                id: newUser._id,
                                name: newUser.name,
                                username: newUser.username
                            }
                        });
                    }
                );
                
            });
        });
        
    } catch (error) {
        res.status(500).send({error: 'Internal server error'});
    }
});

module.exports = router;